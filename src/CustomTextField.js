// FormikSelectField.js
import React from 'react';
import TextField from 'material-ui/TextField';

const CustomTextField = ({
  field: { name, value },
  form: { touched, errors, setFieldValue, setFieldTouched },
  onChange,
  ...otherProps
}) => {
  const handleChange = (event, key, data) => {      
    setFieldTouched(name, true);
    setFieldValue(name, key);

    if (onChange) {
      onChange(event, key, data);
    }
  };

  return (
    <TextField
      errorText={touched[name] && errors[name]}
      value={value}
      onChange={handleChange}
      {...otherProps}
    />
  );
};

export default CustomTextField;
