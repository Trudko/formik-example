import React from 'react';
import {Field} from 'formik'

class FormField extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Field
                name={this.props.name}
                render={(props) => {
                    return (
                        React.cloneElement(
                            this.props.field,
                            props
                        )
                    )
                }}
            />
        )
    }
}

export default FormField;
