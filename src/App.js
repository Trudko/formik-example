import React, { Component } from 'react';
import Yup from 'yup';
import {RaisedButton} from 'material-ui';
import CustomTextField from "./CustomTextField";
import {Field} from 'formik'
import Form from "./Form";
import FormField from "./FormField";
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div>
            <Form
                validationSchema={Yup.object().shape({
                  firstName: Yup.string()
                    .min(3, 'Title must be at least 3 characters long.')
                    .required('Title is required.'),
                    lastName: Yup.string()
                      .min(3, 'Title must be at least 3 characters long.')
                      .required('Last Name is required.'),
                    })
                  }
                 initialValues={{
                   firstName: '',
                   lastName: ''
                 }}
             >

                <div>
                    <FormField
                        name="firstName"
                        field={(
                            <CustomTextField
                            floatingLabelText="FirstName"
                                style={{
                                    marginRight: "10px"
                                }}
                            />
                        )}
                    />
                    <Field
                        name="lastName"
                        render={(props) => {
                            return (<CustomTextField
                                {...props}
                                floatingLabelText="Last Name"
                                style={{
                                    marginRight: "10px"
                                }}

                            />)
                            }
                        }
                    />
                    // <RaisedButton
                    //   type="submit"
                    //   label="Submit"
                    //   primary={true}
                    //   disabled={props.isSubmitting || Object.keys(props.errors).length > 0 || !props.dirty}
                    // />
                </div>

            </Form>

        </div>
      </div>
    );
  }
}

export default App;
